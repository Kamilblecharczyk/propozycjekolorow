import argparse
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn import model_selection
from sklearn.metrics import accuracy_score

from learning import AlsRecommender, BayesianRecommender

fieldnames = ['file', 'color_representation', 'rgb', 'percentage']
arg_parser = argparse.ArgumentParser(description='Evaluate CF implementations')
arg_parser.add_argument('-r', dest='recommendation_size', type=int, required=True,
                        help='How many recommended colors we test')
arg_parser.add_argument('-t', dest='test_size', type=float, required=True,
                        help='0-1, how big part of data should be used for testing')
arg_parser.add_argument('-f', dest='csv_file', type=Path, required=True)


def evaluate(csv_data, colors, test_size):
    input_df = pd.read_csv(csv_data, names=['file_categories', 'color_representation'], usecols=[0, 1], header=0)
    test_size = test_size
    train_data, test_data = model_selection.train_test_split(input_df, test_size=test_size)
    als_score = evaluate_als(train_data, test_data, colors)
    bpr_score = evaluate_bpr(train_data, test_data, colors)
    print('ALS: {0:.3f}'.format(als_score))
    print('BPR: {0:.3f}'.format(bpr_score))


def evaluate_als(train_data, test_data, predicted_colors):
    model = AlsRecommender(train_data)
    results = []
    for color in test_data.values:
        results.extend([c[0] for c in model.recommend(color[1], None, predicted_colors)])
    return accuracy_score(np.repeat(test_data.color_representation, predicted_colors), np.array(results))


def evaluate_bpr(train_data, test_data, predicted_colors):
    model = BayesianRecommender(train_data)
    results = []
    for color in test_data.values:
        results.extend([c[0] for c in model.recommend(color[1], None, predicted_colors)])
    return accuracy_score(np.repeat(test_data.color_representation, predicted_colors), np.array(results))


if __name__ == '__main__':
    parsed_args = arg_parser.parse_args()
    evaluate(parsed_args.csv_file, parsed_args.recommendation_size, parsed_args.test_size)
