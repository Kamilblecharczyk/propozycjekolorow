#!/usr/bin/env python
import argparse
import csv
from multiprocessing.dummy import Pool as ThreadPool
from pathlib import Path

import colorgram
from PIL import Image

from tools.helpers import rgb2hex, low_res_rgb_int, str2bool
from tools.kmeans import KMeansColors

arg_parser = argparse.ArgumentParser(description="Extract Colors")
arg_parser.add_argument('-i', dest='image_folder', type=Path, required=True, help='Path to folder with images')
arg_parser.add_argument('-t', dest='target_excel', required=True, type=Path,
                        default=Path(__file__).absolute() / "output" / 'output.csv',
                        help='Excel file to write, default "./output/output.csv"')
arg_parser.add_argument('-m', dest='mode', choices=['colorgram', 'kmeans', 'kmeans_limited_colors'], required=True,
                        help='Mode of extraction')
arg_parser.add_argument('-c', dest='colors', required=True, type=int, help='Count of extracted colors')
arg_parser.add_argument('-w', dest='workers', required=True, type=int, help='Number of workers used')
arg_parser.add_argument('-d', dest='debug_image', required=False, type=str2bool, default=False,
                        help='Optional, yes or True if you want images representing means')


class ColorExtractor(object):
    fieldnames = ['file', 'color_representation', 'rgb', 'percentage']

    def __init__(self, image_folder, mode, colors, target_excel, workers, progressBar=None, **kwargs):
        self.image_folder_path = Path(image_folder)
        self.mode = mode.lower()
        self.colors = colors
        self.pool = ThreadPool(workers)
        self.output_file_path = Path(target_excel)
        self.save_color_map = kwargs['debug_image'] if 'debug_image' in kwargs else False
        self.included = []
        try:
            with open(self.output_file_path, 'r', newline='') as excel_file:
                self._set_included_images(excel_file)
        except FileNotFoundError:
            pass

    def extract_data(self, progress_bar=None):
        """
        :param progress_bar: optional object with method UpdateBar which functions as external progress bar.
        :return:
        """
        with open(self.output_file_path, 'a', newline='') as excel_file:
            writer = csv.DictWriter(
                excel_file, fieldnames=self.fieldnames
            )

            if len(self.included) == 0:
                writer.writeheader()

            image_files_list = [x for x in self.image_folder_path.iterdir() if x.is_file()]
            images = filter(lambda f: f not in self.included, image_files_list)
            if progress_bar:
                i = 0  # We have to manual count because async can do nth - 2 task last instead of nth.
                progress_bar.UpdateBar(i, max=self._get_file_count())

            for result in self.pool.imap(getattr(self, '_{}_extract'.format(self.mode)), images):
                for row in result:
                    writer.writerow(row)
                if progress_bar:
                    i += 1
                    progress_bar.UpdateBar(i)

            self.pool.close()
            self.pool.join()

    def _get_file_count(self):
        return sum(
            1 for _ in filter(lambda x: x.suffix in ['.jpg', '.jpeg', '.thumbnail'], self.image_folder_path.iterdir()))

    def _set_included_images(self, csv_file):
        reader = csv.DictReader(csv_file, fieldnames=self.fieldnames)
        for row in reader:
            self.included.append(row['file'])

    def _colorgram_extract(self, img_file):
        new_cols = self._cologram_extract(img_file)
        for col in new_cols:
            col['file'] = img_file.filename if isinstance(img_file, Image.Image) else img_file.name
        return new_cols

    def _kmeans_limited_colors_extract(self, img_file):
        """
        Wraps _kmeans_extract, as I had no decent idea how to map images for imap usage.
        :param img_file:
        :return:
        """
        return self._kmeans_extract(img_file, image_color_only=True)

    def _kmeans_extract(self, img_file, image_color_only=False):
        """

        :param img_file:
        :param image_color_only:
        :return: dict for use with CSVWriter
        """
        k_means = KMeansColors(self.colors, img_file)
        new_cols = k_means.parse_image(image_color_only=image_color_only, save_color_map=self.save_color_map)
        for col in new_cols:
            col['file'] = k_means.filename
        return new_cols

    def _cologram_extract(self, filename):
        image_path = self.image_folder_path / filename
        return [self._cologram_color_to_dict(c) for c in colorgram.extract(image_path, self.colors)]

    def _cologram_color_to_dict(self, colorgram_color):
        return {
            'color_representation': low_res_rgb_int(colorgram_color.rgb),
            'rgb': '#' + rgb2hex(colorgram_color.rgb),
            'percentage': colorgram_color.proportion
        }


if __name__ == '__main__':
    args = arg_parser.parse_args()
    extractor = ColorExtractor(**vars(args))
    extractor.extract_data()
