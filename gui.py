import re
from pathlib import Path

import PySimpleGUI as sg

from color_extractor import ColorExtractor
from learning import get_recommender
from tools.helpers import Rgb, low_res_rgb_int
from tools.thumbnailcreator import ThumbnailCreator


# TODO Fix progressbar where TQDM is used inside implicit
# TODO Progress Bar Extract fix.
# TODO Workers Settings

class MainWindow(object):

    def __init__(self):
        menu_layout = [
            [sg.Text('Step Zero: Make images smaller')],
            self.get_thumbnails_menu(),
            [sg.Text('Step One: Extracts colors to file from all images in folders')],
            self.get_extraction_menu(),
            [sg.Text('Step Two: Learn')],
            self.get_learning_menu(),
            [sg.Text('Step Three: Get Recommendations')],
            self.get_color_menu(),
            [
                sg.Button('Exit'),
            ]
        ]

        self.window = sg.Window('Get filename example').Layout(menu_layout)  # not a best solution I guess
        self.recommender = None

    def run_gui(self):
        while True:
            event, values = self.window.Read()
            if event in (None, 'Exit'):
                break
            if event == '_create_thumbnail_':
                try:
                    self.handle_thumbnail_creation(values)
                except FileNotFoundError:
                    sg.Popup('Select image directory')
            elif event == '_extract_colors_':
                self.handle_color_extraction(values)
            elif event == '_load_dataset_':
                self.handle_loading_dataset(values)
            elif event == '_color_set_':
                self.update_color_picker_button(values)
            elif event == '_recommend_colors_':
                self.recommend_colors(values)
            elif event in ['_thumbnails_height_', '_thumbnails_width_']:
                self._validate_int_text(values, key=event, display_key=event + 'state_')

        self.window.Close()

    def handle_thumbnail_creation(self, values):
        try:
            size_tuple = (int(values['_thumbnails_width_']), int(values['_thumbnails_height_']))
            creator = ThumbnailCreator(values['_thumbnails_dir_'], size_tuple)
            creator.create_thumbnails(4, self.window.FindElement('_thumbnails_progress_'))
            sg.Popup('Done')
            self.window.FindElement('_images_dir_').Update(values['_thumbnails_dir_'] + '/thumbnails')
        except (TypeError, ValueError):
            sg.Popup('Please fill image width and height values.')

    def handle_color_extraction(self, values):
        try:
            if Path(values['_images_dir_']).is_dir():
                excel_file = sg.PopupGetFile(message="Save results to:", save_as=True,
                                             file_types=(("CSV Files", "*.csv"), ("All Files", "*.*")))
                extractor = ColorExtractor(
                    image_folder=values['_images_dir_'],
                    mode=values['_extraction_mode_'],
                    colors=4,
                    target_excel=excel_file,
                    workers=4
                )
                extractor.extract_data(progress_bar=self.window.FindElement('_extraction_progress_bar_'))
                self.window.FindElement('_dataset_file_').Update(excel_file)
                sg.Popup('Done')
            else:
                sg.Popup('Select image directory')
        except FileNotFoundError:
            sg.Popup('Select proper target file')
            return
        except TypeError:
            return

    def handle_loading_dataset(self, values):
        try:
            self.recommender = get_recommender(values['_learning_mode_'], values['_dataset_file_'])
            self.window.FindElement('_color_set_').Update(disabled=False)
            self.window.FindElement('_pick_color_').Update(disabled=False)

            sg.Popup('Done')
        except FileNotFoundError:
            sg.Popup('Select Database First')

    def update_color_picker_button(self, values):
        hex_input = values['_color_set_']
        match = re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', hex_input)
        if match:
            self.window.FindElement('_picked_color_').Update(background_color=hex_input)
            self.window.FindElement('_recommend_colors_').Update(disabled=False)

    def recommend_colors(self, values):
        r, g, b = (int(values['_color_set_'].replace('#', '')[i:i + 2], 16) for i in (0, 2, 4))
        base_color = low_res_rgb_int(Rgb(r, g, b))
        if not self.recommender:
            sg.Popup('Load dataset first!')
            return
        colors = self.recommender.recommend_rgb(base_color, 'similar_colors', 4)
        for i, color in enumerate(colors):
            self.window.FindElement('color' + str(i + 1)).Update(
                background_color='#' + color[0]
            )
            self.window.FindElement('rgb_color' + str(i + 1)).Update(value='#' + color[0])
            self.window.FindElement('val_color' + str(i + 1)).Update(value='Score:' + str(color[1]))
        sg.Popup('Done')

    def _validate_int_text(self, values, key, display_key):
        try:
            int(values[key])
            #  window default is None for default color scheme and we cannot reset to that.
            self.window.FindElement(display_key).Update(value='', background_color='green', )
        except (TypeError, ValueError):
            self.window.FindElement(display_key).Update(value='Please enter integer', background_color='red')

    @staticmethod
    def get_thumbnails_menu():
        return [
            sg.Column([
                [
                    sg.Text('', size=(23, 1)),
                    sg.Text('Images Width', size=(20, 1)),
                    sg.Text('Images Height', size=(20, 1)),
                    sg.Text('Images Folder', size=(20, 1)),
                ],
                [
                    sg.ProgressBar(max_value=1, key='_thumbnails_progress_', size=(14, 1)),
                    sg.InputText('', key='_thumbnails_width_', enable_events=True, size=(20, 1)),
                    sg.InputText('', key='_thumbnails_height_', enable_events=True, size=(20, 1)),
                    sg.InputText('', key='_thumbnails_dir_', size=(30, 1)),
                ],
                [
                    sg.Button('Create Thumbnails', key='_create_thumbnail_', size=(18, 1)),
                    sg.Text('', key='_thumbnails_width_state_', size=(20, 1)),
                    sg.Text('', key='_thumbnails_height_state_', size=(20, 1)),
                    sg.FolderBrowse('Pick Folder', target='_thumbnails_dir_', size=(28, 1))
                ]
            ])
        ]

    @staticmethod
    def get_extraction_menu():
        return [
            sg.Column([
                [
                    sg.Text('', size=(21, 1)),
                    sg.Text('Extraction Method', size=(22, 1)),
                    sg.Text('Images Folder', size=(20, 1)),
                ],
                [
                    sg.ProgressBar(max_value=1, key='_extraction_progress_bar_', size=(14, 1)),
                    sg.InputCombo(['kmeans_limited_colors', 'kmeans', 'colorgram'], key='_extraction_mode_',
                                  size=(20, 1)),
                    sg.InputText('', key='_images_dir_', size=(31, 1)),
                ],
                [
                    sg.Button('Extract Colors', key='_extract_colors_', size=(18, 1)),
                    sg.Text('', size=(22, 1)),
                    sg.FolderBrowse('Pick folder', target='_images_dir_', size=(28, 1)),
                ]
            ])
        ]

    @staticmethod
    def get_learning_menu():
        return [
            sg.Column([
                [
                    sg.Text('', size=(21, 1)),
                    sg.Text('CF method', size=(20, 1)),
                    sg.Text('CSV file', size=(30, 1))
                ],
                [
                    sg.ProgressBar(max_value=1, key='_learning_progress_bar_', size=(14, 1)),
                    sg.InputCombo(['ALS', 'BPR'], key='_learning_mode_', size=(20, 1)),
                    sg.InputText('', key='_dataset_file_', size=(31, 1))
                ],
                [
                    sg.Button('Load learning Database', key='_load_dataset_', size=(18, 1)),
                    sg.Text('', size=(22, 1)),
                    sg.FileBrowse(target='_dataset_file_', size=(28, 1)),
                ]

            ])
        ]

    @staticmethod
    def get_color_menu():
        return [
            sg.Column([
                [
                    sg.Text('', size=(21, 1)),
                    sg.Text('Pick Color', size=(21, 1)),
                    sg.Text('Output Color 1', size=(15, 1)),
                    sg.Text('Output Color 2', size=(15, 1)),
                    sg.Text('Output Color 3', size=(15, 1)),
                    sg.Text('Output Color 4', size=(15, 1)),
                ], [
                    sg.Text('', size=(21, 1)),
                    sg.Text('', key='_picked_color_', size=(20, 1)),

                    sg.Text('', key='color1', size=(15, 1)),
                    sg.Text('', key='color2', size=(15, 1)),
                    sg.Text('', key='color3', size=(15, 1)),
                    sg.Text('', key='color4', size=(15, 1)),
                ], [
                    sg.Text('', size=(21, 1)),
                    sg.InputText('', key='_color_set_', disabled=True, enable_events=True, size=(20, 1)),
                    sg.Text('', key='val_color1', size=(15, 1)),
                    sg.Text('', key='val_color2', size=(15, 1)),
                    sg.Text('', key='val_color3', size=(15, 1)),
                    sg.Text('', key='val_color4', size=(15, 1)),
                ], [
                    sg.Button('Recommend Colors', disabled=True, key='_recommend_colors_', size=(18, 1)),
                    sg.ColorChooserButton('ColorPicker', disabled=True, target='_color_set_', key='_pick_color_',
                                          size=(17, 1)),
                    sg.InputText('', key='rgb_color1', size=(15, 1)),
                    sg.InputText('', key='rgb_color2', size=(15, 1)),
                    sg.InputText('', key='rgb_color3', size=(15, 1)),
                    sg.InputText('', key='rgb_color4', size=(15, 1)),
                ]
            ])
        ]


if __name__ == '__main__':
    window = MainWindow()
    window.run_gui()
