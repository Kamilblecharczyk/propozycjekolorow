import argparse
from pathlib import Path

import implicit
import numpy as np
import pandas as pd
import scipy

from tools.helpers import Rgb, low_res_rgb_int, upgrade_resolution

arg_parser = argparse.ArgumentParser(description="Recommends colors")
arg_parser.add_argument('-f', dest='csv_file', type=Path, required=True, help='CSV file containing learning data')
arg_parser.add_argument('-c', dest='color', help='Color for which you want recommendations in #A1B2C3 format',
                        required=True)
arg_parser.add_argument('-r', dest='recommender', choices=['ALS', 'BPR'])
arg_parser.add_argument('-m', dest='mode', choices=['similar_colors', 'recommendation'],
                        help='Similar colors is list of similar items, Recommend is new recommendation')
arg_parser.add_argument('-n', dest='color_count', help='Count of colors you want to use.', type=int)


class Recommender(object):

    def __init__(self, input_file):
        if isinstance(input_file, pd.DataFrame):
            self.df = input_file
        else:
            self.df = pd.read_csv(input_file, names=['file_categories', 'color_representation'], usecols=[0, 1],
                                  header=0)
        self.csr_matrix = None
        self.users_dict = {}
        self.fitted = False

    def create_recommendation_user(self, color_representation):
        new_info = pd.DataFrame([['recommendation_user', color_representation]],
                                columns=['file_categories', 'color_representation'])
        self.df = self.df.append(new_info)

    def learn(self):
        if not self.fitted:
            self.df['file_categories'] = self.df['file_categories'].astype("category")  # convert names to numbers
            self.users_dict = dict(zip(self.df.file_categories.cat.categories, self.df.file_categories.cat.codes))

            coo_m = scipy.sparse.coo_matrix(
                (np.ones(self.df.shape[0]),  # Is this wrong shape?
                 (self.df.color_representation.copy(), self.df.file_categories.cat.codes.copy()))
            )
            self.csr_matrix = coo_m.tocsr()
            self.model.fit(self.csr_matrix)
            self.fitted = True


class AlsRecommender(Recommender):

    def __init__(self, input_file):
        super().__init__(input_file)
        self.model = implicit.als.AlternatingLeastSquares(factors=32)

    def recommend(self, color, mode, recommendation_size):
        self.learn()
        results = self.model.similar_items(color, recommendation_size)
        return results

    def recommend_rgb(self, *args):
        return [(upgrade_resolution(color[0]), color[1]) for color in self.recommend(*args)]

    def recommend_cli(self, *args):
        return ["{} - score {}".format(upgrade_resolution(similar[0]), similar[1]) for similar in self.recommend(*args)]


class BayesianRecommender(Recommender):
    def __init__(self, input_file):
        super().__init__(input_file)
        self.model = implicit.bpr.BayesianPersonalizedRanking(factors=63)

    def recommend(self, color, mode, recommendation_size=4):
        self.learn()
        results = self.model.similar_items(color, recommendation_size)
        return results

    def recommend_rgb(self, *args):
        return [(upgrade_resolution(color[0]), color[1]) for color in self.recommend(*args)]

    def recommend_cli(self, *args):
        return ["{} - score {}".format(upgrade_resolution(similar[0]), similar[1]) for similar in self.recommend(*args)]


def get_recommender(recommender_name, csv_file):
    if recommender_name.upper() == 'ALS':
        return AlsRecommender(csv_file)
    elif recommender_name.upper() == 'BPR':
        return BayesianRecommender(csv_file)
    else:
        raise ValueError


if __name__ == '__main__':
    parsed_args = arg_parser.parse_args()
    r, g, b = ((parsed_args.color[i:i + 2], 16) for i in (0, 2, 4))
    base_color = Rgb(r=r[1], g=g[1], b=b[1])
    recommender = get_recommender(parsed_args.recommender, parsed_args.csv_file)
    results = recommender.recommend_cli(low_res_rgb_int(base_color), parsed_args.mode, parsed_args.color_count)
    print(results)
