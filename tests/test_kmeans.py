import os
import unittest
from pathlib import Path
from unittest import mock

import math
from PIL import Image

from tools.helpers import Rgb
from tools.kmeans import KMean, KMeansColors, ColorPoint, TooManyKMeansError


class TestKMeansColors(unittest.TestCase):

    def test_get_color_point(self):
        file_path = Path('./get_color_point.png')
        test_image = Image.new(mode="RGB", size=(2, 2), color=(255, 255, 255))
        test_image.putpixel(xy=(0, 0), value=(255, 0, 0))
        test_image.save(file_path)

        k_mean = KMeansColors(3, file_path)
        self.assertEqual(k_mean._get_color_point(0, 0).rgb, Rgb(1.0, 0, 0))
        self.assertEqual(k_mean._get_color_point(1, 1).rgb, Rgb(1.0, 1.0, 1.0))

        os.remove(file_path)

    def test_too_many_k_raises(self):
        file_path = Path('./get_color_point.png')
        test_image = Image.new(mode="RGB", size=(2, 2), color=(255, 255, 255))
        test_image.putpixel(xy=(0, 0), value=(255, 0, 0))

        test_image.save(file_path)

        with self.assertRaises(TooManyKMeansError):
            KMeansColors(4, file_path)
        os.remove(file_path)

    @mock.patch.object(KMeansColors, '_get_color_point', return_value=None)  # FIXME
    @mock.patch.object(KMean, 'hls_distance', return_value=None)
    def test_get_colors(self, f1, f2):
        # test_image = Image.new(mode="RGB", size=(2, 2), color=(255, 255, 255))
        # kmean = KMeansColors(4, test_image, 'target_file.jpg', image_color_only=False)
        # f1.assertCalledWith()
        pass


class TestKMean(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.test_pixel = ColorPoint.from_hls_point(1, 1, 0, 0, 0)

    def test_add_point(self):
        """
        We test on HLS as most math will be done in that space.
        :return:
        """
        pink_pixel = ColorPoint.from_hls_point(1, 1, 1, 0, 1)
        red_pixel = ColorPoint.from_hls_point(5, 5, 1, 0, 0)
        target_pixel = ColorPoint.from_hls_point(3, 3, 1, 0, 0.5)
        k_mean = KMean(pink_pixel)
        k_mean.add_point(red_pixel)

        self.assertAlmostEqual(k_mean.center_point.rgb, target_pixel.rgb)
        self.assertAlmostEqual(k_mean.center_point.x, target_pixel.x)
        self.assertAlmostEqual(k_mean.center_point.y, target_pixel.y)
        self.assertEqual(len(k_mean.points), 2)
        self.assertIn(pink_pixel, k_mean.points)
        self.assertIn(red_pixel, k_mean.points)

    def test_extend_average(self):
        k_mean = KMean(self.test_pixel)
        k_mean.points.append('fake_pixel')
        #  Set up end here
        extended = k_mean._extend_average(1, 0)
        self.assertAlmostEqual(0.5, extended)
        extended = k_mean._extend_average(0, 0)
        self.assertEqual(0, extended)

    def test_hls_distance_zero_point(self):
        """
        Simple test to verify if we are using proper distance
        :return:
        """
        k_mean = KMean(self.test_pixel)
        distance = k_mean.hls_distance((4, 9, 16))
        self.assertEqual(math.sqrt(353), distance)

    def test_get_percentage(self):
        k_mean = KMean(self.test_pixel)
        k_mean.points = [1, 2, 3, 4, 5]
        self.assertEqual(k_mean.percentage_covered(10), 0.5)
