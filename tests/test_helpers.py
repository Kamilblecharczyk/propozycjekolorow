import itertools
from argparse import ArgumentTypeError
from collections import namedtuple
from unittest import TestCase

from ddt import data, ddt, unpack

from tools.helpers import euclidean_distance, str2bool, Rgb, rgb2hex, downgrade_resolution, low_res_rgb_int, \
    convert_to_256_scale, upgrade_resolution


@ddt
class TestHelperFunctions(TestCase):

    @data('yes', 'true', 't', 'y')
    def test_str2bool_truthy_strings(self, word):
        for text in map(''.join, itertools.product(*((c.upper(), c.lower()) for c in word))):
            self.assertEqual(str2bool(text), True)

    @data('no', 'false', 'f', 'n')
    def test_str2bool_falsy_strings(self, word):
        for text in map(''.join, itertools.product(*((c.upper(), c.lower()) for c in word))):
            self.assertEqual(str2bool(text), False)

    def test_str2bool_number_strings(self):
        self.assertEqual(str2bool('1'), True)
        self.assertEqual(str2bool('0'), False)

    @data('yesn', 'asd', '123', 'fualse')
    def test_str2bool_raises_wrong_string(self, color):
        with self.assertRaises(ArgumentTypeError):
            str2bool(color)

    @unpack
    @data(
        ([67, 89, 130], '435982'),
        ((1, 5, 10), '01050A'),
        (Rgb(r=128, g=23, b=123), '80177B')
    )
    def test_rgb2hex(self, color, expected):
        self.assertEqual(rgb2hex(color), expected)

    @unpack
    @data(
        {'color_value': 33, 'expected': 2},
        {'color_value': 4, 'expected': 0},
        {'color_value': 17, 'expected': 1},
        {'color_value': 162, 'expected': 10},
        {'color_value': 0.1, 'expected': 1},
        {'color_value': 0.99, 'expected': 15},
        {'color_value': 0.2, 'expected': 3}
    )
    def test_downgrade_resolution(self, color_value, expected):
        self.assertEqual(expected, downgrade_resolution(color_value))

    @unpack
    @data(
        {'color_value': Rgb(r=1.0, g=0.625, b=0.001), 'expected': 4000}
    )
    def test_low_res_rgb_int(self, color_value, expected):
        self.assertEqual(expected, low_res_rgb_int(color_value))

    @unpack
    @data(
        {'input_float': 1.0, 'expected_int': 255},
        {'input_float': 0.1, 'expected_int': 25}
    )
    def test_convert_to_256_scale(self, input_float, expected_int):
        self.assertEqual(expected_int, convert_to_256_scale(input_float))

    @unpack
    @data(
        {'downgraded_color': 4000, 'expected_rgb': 'F0A000'}  # Not #FFA000 because we round down
    )
    def test_upgrade_resolution(self, downgraded_color, expected_rgb):
        self.assertEqual(expected_rgb, upgrade_resolution(downgraded_color))


class TestEuclideanDist(TestCase):

    def test_named_tuple(self):
        testNamedTuple = namedtuple('XY', ('x', 'y'))
        point1 = testNamedTuple(1, 3)
        point2 = testNamedTuple(4, 7)
        distance = euclidean_distance(point1, point2)
        self.assertEqual(distance, 5)

    def test_tuple(self):
        point1 = (1, 3)
        point2 = (4, 7)
        distance = euclidean_distance(point1, point2)
        self.assertEqual(distance, 5)

    def test_1d_pts(self):
        point1 = [1]
        point2 = [10]
        distance = euclidean_distance(point1, point2)
        self.assertEqual(distance, 9)

    def test_2d_pts(self):
        point1 = [1, 3]
        point2 = [4, 7]
        distance = euclidean_distance(point1, point2)
        self.assertEqual(distance, 5)

    def test_3d_pts(self):
        point1 = [1, 2, 4]
        point2 = [1, 22, 25]
        distance = euclidean_distance(point1, point2)
        self.assertEqual(distance, 29)

    def test_mismatched_dimensions(self):
        point1 = [2, 4]
        point2 = [1, 2, 3, 4, 5, 6]
        distance = euclidean_distance(point1, point2)
        self.assertIsNone(distance)
