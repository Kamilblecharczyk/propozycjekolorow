import colorsys
from pathlib import Path
from random import Random

from PIL import Image, ImageDraw

from tools.helpers import euclidean_distance, Rgb, Hls, Hsv, Yiq, low_res_rgb_int, rgb2hex, convert_to_256_scale


class TooManyKMeansError(Exception):
    """
    Raised if you there is KMean for each pixel in image or above that value.
    """


class ColorPoint(object):
    """
    Object representing pixel by position and color.
    """

    def __init__(self, pos_x, pos_y, r, g, b):
        self.x = pos_x
        self.y = pos_y
        self.rgb = Rgb(r, g, b)

    def point_distance(self, coordinate):
        """
        Euclidean distance between point and coordinate
        :param coordinate: iterable with len equal 2
        :return:
        """
        return euclidean_distance((self.x, self.y), coordinate)

    @classmethod
    def from_hls_point(cls, pos_x, pos_y, h, l, s):
        # FIXME .from_hls_point(pos, hls) maybe?
        return cls(pos_x, pos_y, *colorsys.hls_to_rgb(h, l, s))

    @property
    def hls(self):
        return Hls(*colorsys.rgb_to_hls(*self.rgb))

    @property
    def yiq(self):
        return Yiq(*colorsys.rgb_to_yiq(*self.rgb))

    @property
    def hsv(self):
        return Hsv(*colorsys.rgb_to_hsv(*self.rgb))

    @property
    def hex_rgb(self):
        return '#' + rgb2hex(self.rgb_256_scale)

    @property
    def rgb_256_scale(self):
        return Rgb(r=convert_to_256_scale(self.rgb.r),
                   g=convert_to_256_scale(self.rgb.g),
                   b=convert_to_256_scale(self.rgb.b))

    @property
    def low_res_representation(self):
        """
        Custom format.
        Representation using single number created from 16 instead of 256 colors,
        used for recommendation models, as we
        don't have really big data.
        :return: int
        """
        return low_res_rgb_int(self.rgb)

    def color_distance(self, color, color_mode):
        """
        Euclidean distance on 3D color space
        :color: iterable representing color
        :color_mode: which color space of ColorPoint you want to measure distance to
        :return: float
        """
        x, y, z = getattr(self, color_mode)
        return euclidean_distance((x, y, z), color)


class KMean(object):

    def __init__(self, point):
        """
        :param point: starting ColorPoint instance
        """
        self.points = [point]
        self.center_point = point

    @property
    def centroid_h(self):
        return self.center_point.hls.h

    @property
    def centroid_s(self):
        return self.center_point.hls.s

    @property
    def centroid_l(self):
        return self.center_point.hls.l

    @property
    def centroid_hls(self):
        return self.center_point.hls

    def add_point(self, point):
        """
        Adds new point to mean.
        :param point:
        :return:
        """
        self.points.append(point)
        new_pos_x = self._extend_average(self.center_point.x, point.x)
        new_pos_y = self._extend_average(self.center_point.y, point.y)
        new_h = self._extend_average(self.centroid_h, point.hls.h)
        new_s = self._extend_average(self.centroid_s, point.hls.s)
        new_l = self._extend_average(self.centroid_l, point.hls.l)

        self.center_point = ColorPoint.from_hls_point(
            pos_x=new_pos_x,
            pos_y=new_pos_y,
            h=new_h,
            l=new_l,
            s=new_s
        )

    def _extend_average(self, average, new_value):
        return (average * (len(self.points) - 1) + new_value) / (len(self.points))

    def hls_distance(self, hls):
        """
        Euclidean distance on 3D color space
        :hls: iterable with h, l, s values
        :return:
        """
        return euclidean_distance(self.centroid_hls, hls)

    def percentage_covered(self, total_area_count):
        """
        Returns how many percent of area sized total_pixels is covered
        by this KMean.
        :param total_area_count:
        :return:
        """
        return len(self.points) / total_area_count

    def get_point_closest_to_mean(self):
        """
        Finds ColorPoint from cloud of included points which is closest to centroid.
        :return:
        """
        return sorted(self.points, key=lambda x: self.hls_distance(x.hls))[0]


class KMeansColors(object):
    """
    Extracts K means of colors for images.
    """

    def __init__(self, means_count, file):
        """
        :param means_count: Amount of means used.
        :param file: PIL.Image or Path
        """
        self.number_of_colors = means_count
        self.filename = file.filename if isinstance(file, Image.Image) else file.name
        self.image = file if isinstance(file, Image.Image) else Image.open(file)
        if self.image.mode not in ('RGB', 'RGBA', 'RGBa'):
            self.image = self.image.convert('RGB')
        self.pixels = self.image.load()
        self.width, self.height = self.image.size
        self.total_pixels = self.width * self.height
        self._set_starting_means()

    def parse_image(self, image_color_only=False, save_color_map=False):
        results = self._get_colors(image_color_only)
        if save_color_map:
            self._save_color_map()
        return results

    def _set_starting_means(self):
        if self.width * self.height <= self.number_of_colors:
            raise TooManyKMeansError("You have more KMeans than pixels in image")

        color_points = []
        while len(color_points) < self.number_of_colors:
            starting_color_point = self._get_random_colorpoint()
            if starting_color_point not in color_points:
                color_points.append(starting_color_point)
        self.k_means = [KMean(color_point) for color_point in color_points]

    def _get_random_colorpoint(self):
        x = Random().randint(0, self.width - 1)
        y = Random().randint(0, self.height - 1)
        return self._get_color_point(x, y)

    def _get_color_point(self, x, y):
        """
        Gets color from point on image
        :param x:
        :param y:
        :return:
        """
        r, g, b = self.pixels[x, y][:3]
        r = r / 255
        g = g / 255
        b = b / 255
        return ColorPoint(x, y, r, g, b)

    def _get_colors(self, image_color_only=False):
        """
        Extracts colors using KMeans algorithm.
        :image_color_only: should returned color be taken only from colors in image space.
        :return: list of tuples (r, g, b, percent_of_pixels)
        """
        for y in range(self.height):
            for x in range(self.width):
                new_color_point = self._get_color_point(x, y)
                target_k_mean = sorted(self.k_means, key=lambda a: a.hls_distance(new_color_point.hls))[0]
                target_k_mean.add_point(new_color_point)
        if image_color_only:
            return self._image_only_results()
        return self._any_color_results()

    def _image_only_results(self):
        """
        Finds point which is closed to each
        :return:
        """
        results = []
        for k in self.k_means:
            closest_point = k.get_point_closest_to_mean()
            results.append(
                {
                    'color_representation': closest_point.low_res_representation,
                    'rgb': closest_point.hex_rgb,
                    'percentage': k.percentage_covered(self.total_pixels)
                }
            )
        return results

    def _any_color_results(self):
        return [
            {
                'color_representation': k.center_point.low_res_representation,
                'rgb': k.center_point.hex_rgb,
                'percentage': k.percentage_covered(self.total_pixels)
            } for k in self.k_means
        ]

    def _draw_color_map(self, image):
        """
        Draws on Image map of which pixels were in which color group.
        :param image: PIL.Image
        """
        draw = ImageDraw.Draw(image)
        for k_mean in self.k_means:
            rgb = k_mean.center_point.rgb
            fill_color = (min(int(rgb.r * 256), 255), min(int(rgb.g * 256), 255), min(int(rgb.b * 256), 255))
            draw.point([(point.x, point.y) for point in k_mean.points], fill=fill_color)

    def _save_color_map(self):
        """
        Creates Image representing which parts of image were assigned to each mean.
        :return:
        """
        im = Image.new(self.image.mode, (self.width, self.height))
        self._draw_color_map(im)
        target_directory = Path('./color_maps')
        if not target_directory.exists():
            target_directory.mkdir(parents=True)
        im.save(target_directory / "{}".format(self.filename + 'thumb.png'), "PNG")
