import argparse
from multiprocessing.dummy import Pool as ThreadPool
from pathlib import Path

from PIL import Image

arg_parser = argparse.ArgumentParser(description="Create thumbnails for given directory, in new folder './thumbnails/'")
arg_parser.add_argument('-i', dest='image_folder', type=Path, required=True, help='Path to folder with images')
arg_parser.add_argument('-w', dest='workers', required=True, type=int, help='Number of workers used')
arg_parser.add_argument('-s', dest='size', required=True, type=int, default=256, help='Thumbnail size in pixels')


class ThumbnailCreator(object):

    def __init__(self, path, thumbnail_size):
        self.path = Path(path)
        self.thumbnail_size = (thumbnail_size, thumbnail_size) if type(thumbnail_size) is int else thumbnail_size
        self.file_count = self._get_file_count()
        print('fc {}'.format(self.file_count))

    def _get_file_count(self):
        return sum(1 for _ in filter(lambda x: x.suffix in ['.jpg', '.jpeg'], self.path.iterdir()))

    def create_thumbnails(self, workers, progress_bar=None):
        """
        Creates thumbnails from path in new directory "thumbnails".
        :param workers:
        :param progress_bar: optional object with method UpdateBar which functions as external progress bar.
        :return:
        """
        with ThreadPool(workers) as pool:
            if progress_bar:
                i = 0  # We have to manual count because async can do nth - 2 task last instead of nth.
                progress_bar.UpdateBar(i, max=self.file_count)
                for _ in pool.imap(self._create_thumbnail, self._image_filename_generator()):
                    i += 1
                    progress_bar.UpdateBar(i)
            else:
                pool.imap(self._create_thumbnail, self._image_filename_generator())
            pool.close()
            pool.join()
        return True

    def _image_filename_generator(self):
        for filename in self.path.iterdir():
            if filename.is_file() and filename.suffix in ['.jpg', '.jpeg']:
                yield self.path / filename

    def _create_thumbnail(self, file):
        size = self.thumbnail_size
        with Image.open(file) as im:
            filename = file.stem
            im.thumbnail(size)
            thumbnail_path = file.parent / 'thumbnails'
            try:
                thumbnail_path.mkdir()
            except FileExistsError:
                pass
            new_file = thumbnail_path.joinpath(filename + '.thumbnail.jpg')
            im.save(new_file, "JPEG")
        return


if __name__ == '__main__':
    args = arg_parser.parse_args()
    creator = ThumbnailCreator(args.image_folder, args.size)
    creator.create_thumbnails(args.workers)
