import argparse
import csv
from builtins import zip
from collections import namedtuple

import math


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def euclidean_distance(p, q):
    """
    Euclidean distance between two points, each represented by iterable.
    Handles any number of dimensions, as long as points are represented by same count of dimensions.
    :param p: first point
    :param q: second point
    :return: float
    """
    if len(p) != len(q):
        return None
    partial_distances = [(axis_pos[0] - axis_pos[1]) ** 2 for axis_pos in list(zip(p, q))]
    return math.sqrt(sum(partial_distances))


Rgb = namedtuple('Rgb', ('r', 'g', 'b'))
Hls = namedtuple('Hls', ('h', 's', 'l'))
Hsv = namedtuple('Hsv', ('h', 's', 'v'))
Yiq = namedtuple('Yiq', ('y', 'i', 'q'))


def rgb2hex(color):
    """Converts a list or tuple of color to an RGB string
    Args:
        color (list|tuple): the list or tuple of integers (e.g. (127, 127, 127))

    Returns:
        str:  the rgb string
    """
    return f"{''.join(f'{hex(c)[2:].upper():0>2}' for c in color)}"


def hex2rgb(color):
    color = color.replace('#', '')
    return Rgb(*(int(color[i:i + 2], 16) for i in (0, 2, 4)))


def hilo(a, b, c):
    if c < b: b, c = c, b
    if b < a: a, b = b, a
    if c < b: b, c = c, b
    return a + c


def complement(r, g, b):
    k = hilo(r, g, b)
    return Rgb(*(k - u for u in (r, g, b)))


def downgrade_resolution(color):
    """
    Downgrades colors from 256 values to 16 values.
    :param color: int or float
    :return: int
    """
    if type(color) is int:  # We are in 256 color space
        int_c = color
    else:  # we are in .0 - 1.0 space.
        int_c = convert_to_256_scale(color)
    return int((int_c - (int_c % 16)) / 16)


def low_res_rgb_int(rgb):
    return downgrade_resolution(rgb.r) * 256 + \
           downgrade_resolution(rgb.g) * 16 + \
           downgrade_resolution(rgb.b)


def convert_to_256_scale(value):
    return min(int(value * 256), 255)


def flatten_csv(source_csv, target_csv):
    """
    As we probably need different structure of csv file in collaborative filtering:
    Quick parser is written here.
    Defacto Depracted.
    :return:
    """
    with open(source_csv, 'r') as source:
        reader = csv.reader(source)
        with open(target_csv, 'w') as output:
            writer = csv.writer(output)
            writer.writerow(['filename', 'color'])
            reader.__next__()
            for row in reader:
                for color in range(4):
                    r, g, b = downgrade_resolution(row[color * 5 + 1]), downgrade_resolution(
                        row[color * 5 + 2]), downgrade_resolution(row[color * 5 + 3])
                    writer.writerow([row[0], r * 256 + g * 16 + b])


def upgrade_resolution(downgraded_color):
    r = math.floor(downgraded_color / 256) * 16
    g = math.floor((downgraded_color % 256) / 16) * 16
    b = math.floor(((downgraded_color % 256) % 16) / 1) * 16
    return rgb2hex((r, g, b))
